/*
 * MCP2515.cpp
 *
 *  Created on: 5 avr. 2016
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         MCP2515.cpp
 *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier MCP2515.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */
#include "MCP2515.h"

MCP2515Reg::MCP2515Reg() {
	// TODO Auto-generated constructor stub

}

MCP2515Reg::~MCP2515Reg() {
	// TODO Auto-generated destructor stub
}

void MCP2515Reg::setRegistre(unsigned char val){
	this->valregistre=val;
}

unsigned char MCP2515Reg::getRegistre(){
	return this->valregistre;
}
