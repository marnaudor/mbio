/********************************************************************************
** Form generated from reading UI file 'RegMCP2515Dlg.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGMCP2515DLG_H
#define UI_REGMCP2515DLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegMCP2515Dlg
{
public:
    QWidget *centralwidget;
    QPushButton *Lire;
    QComboBox *Registre;
    QLineEdit *lineEdit;
    QLCDNumber *lcdNumber;
    QPushButton *Ecrire;
    QComboBox *Registreext;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *RegMCP2515Dlg)
    {
        if (RegMCP2515Dlg->objectName().isEmpty())
            RegMCP2515Dlg->setObjectName(QStringLiteral("RegMCP2515Dlg"));
        RegMCP2515Dlg->resize(501, 211);
        centralwidget = new QWidget(RegMCP2515Dlg);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        Lire = new QPushButton(centralwidget);
        Lire->setObjectName(QStringLiteral("Lire"));
        Lire->setGeometry(QRect(60, 20, 98, 27));
        Registre = new QComboBox(centralwidget);
        Registre->setObjectName(QStringLiteral("Registre"));
        Registre->setGeometry(QRect(380, 20, 78, 27));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(50, 90, 113, 27));
        lcdNumber = new QLCDNumber(centralwidget);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        lcdNumber->setGeometry(QRect(170, 20, 64, 23));
        Ecrire = new QPushButton(centralwidget);
        Ecrire->setObjectName(QStringLiteral("Ecrire"));
        Ecrire->setGeometry(QRect(60, 120, 98, 27));
        Registreext = new QComboBox(centralwidget);
        Registreext->setObjectName(QStringLiteral("Registreext"));
        Registreext->setGeometry(QRect(380, 90, 79, 23));
        RegMCP2515Dlg->setCentralWidget(centralwidget);
        menubar = new QMenuBar(RegMCP2515Dlg);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 501, 20));
        RegMCP2515Dlg->setMenuBar(menubar);
        statusbar = new QStatusBar(RegMCP2515Dlg);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        RegMCP2515Dlg->setStatusBar(statusbar);

        retranslateUi(RegMCP2515Dlg);

        QMetaObject::connectSlotsByName(RegMCP2515Dlg);
    } // setupUi

    void retranslateUi(QMainWindow *RegMCP2515Dlg)
    {
        RegMCP2515Dlg->setWindowTitle(QApplication::translate("RegMCP2515Dlg", "MainWindow", 0));
        Lire->setText(QApplication::translate("RegMCP2515Dlg", "Lire Registre", 0));
        Ecrire->setText(QApplication::translate("RegMCP2515Dlg", "Ecrire Registre", 0));
    } // retranslateUi

};

namespace Ui {
    class RegMCP2515Dlg: public Ui_RegMCP2515Dlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGMCP2515DLG_H
