/**
 *  Copyright (C) 2016  michel  (michel@btssn.delattre.com)
 *  @file         RegMCP2515Dlg.h
 *  @brief        
 *  @version      0.1
 *  @date         28 mars 2016 23:49:42
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                RegMCP2515Dlg.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _REGMCP2515DLG_H
#define _REGMCP2515DLG_H
#include "BusCAN.h"
// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++

// Includes qt
#include <QMainWindow>

// Includes application
#include "ui_RegMCP2515Dlg.h"


/** @brief 
 *  Description detaillee de la classe.
 */
class RegMCP2515Dlg:public QMainWindow, private Ui_RegMCP2515Dlg
{
	Q_OBJECT
	BusCAN *mcan;
public :
    /**
     * Constructeur
     */
    RegMCP2515Dlg(BusCAN *,QObject *parent = 0);
    /**
     * Destructeur
     */
    ~RegMCP2515Dlg();
    void AfficheReg(unsigned char);
    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;

    // Methodes privees
signals:
// signaux générés    
private slots:
    // definition des slots privées
  void  onLire();
  void  onEcrire();
private slots:
     // definition des slots publics
    
};

// Methodes publiques inline
// ex :
// inline void RegMCP2515Dlg::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type RegMCP2515Dlg::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _REGMCP2515DLG_H

