/**
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         USBTin.cpp
 *  @brief        
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier USBTin.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "USBTin.h"

/**
 * Constructeur
 */
USBTin::USBTin()
{
	this->setupUi(this);
 	connect(this->Ouvrir,SIGNAL(clicked()),this,SLOT(onOuvrir()));
 	connect(this->Connecter,SIGNAL(clicked()),this,SLOT(onConnecter()));
        connect(this->pBDeconnect,SIGNAL(clicked()),this,SLOT(onDeconnect()));
        connect(this->Envoie,SIGNAL(clicked()),this,SLOT(onEnvoie()));
        connect(this->Deconnect,SIGNAL(clicked()),this,SLOT(onDeconnect()));
 	connect(this->Effacer,SIGNAL(clicked()),this,SLOT(onEffacer()));
 	connect(this->Sauvegarder,SIGNAL(clicked()),this,SLOT(onSauvegarder()));
 	connect(this->Extended,SIGNAL(clicked()),this,SLOT(onCheckExtended()));
 	connect(this->Rtr,SIGNAL(clicked()),this,SLOT(onRTR()));
 	connect(this->MCP2515,SIGNAL(clicked()),this,SLOT(onMCP2515()));
	connect(this->Acceptance,SIGNAL(clicked()),this,SLOT(onAcceptanceMask()));
	connect(this->AcceptanceFiltre,SIGNAL(clicked()),this,SLOT(onAcceptanceFiltre()));
	connect(this->Vitesse,SIGNAL(currentIndexChanged(int)),this,SLOT(onVitesse(int)));
        //mcp=new MCP2515Reg();
 	this->Connecter->setEnabled(false);
 	this->Deconnect->setEnabled(false);
	this->Effacer->setEnabled(false);
 	this->Envoie->setEnabled(false);
 	this->Ouvrir->setEnabled(false);
 	this->Sauvegarder->setEnabled(false);
 	this->MCP2515->setEnabled(false);
 	this->Vitesse->setEnabled(false);
	int i;
	numeromessage=0;
	mode=STD;
//	char val;
	opencan=false;
	openport=false;
	checkextended=false;
	checkrtr=false;
	QVector<QString> dev;
	mondevice=new Device();
	bool devtrouve=mondevice->getDevices(&dev);
if (devtrouve){
	for(i=0;i<dev.length();i++)
		this->PortCom->addItem(dev[i]);

	this->Vitesse->addItem("1000k");
	this->Vitesse->addItem("250k");
	this->Vitesse->addItem("125k");
	this->Vitesse->addItem("500k");

	QStringList listeHeader;
	listeHeader << "s/e" << "identifiant"<<"l"<<"Data";
	QStringListModel *modeleHeader = new QStringListModel(listeHeader);
	QHeaderView *vueHeader = new QHeaderView(Qt::Horizontal);
	vueHeader->setModel(modeleHeader);
	this->tableView->setHorizontalHeader(vueHeader);
	//vuePlug_in->setHorizontalHeader(vueHeader);
	//vuePlug_in->setGeometry(0, 80, 400, 200);
	modelePlug_in = new QStandardItemModel(0, 4);
	modelePlug_in->setHorizontalHeaderLabels(listeHeader);
	//m_selectCategoryTableWidget->header()->setStretchLastSection(false);
	//m_selectCategoryTableWidget->header()->setResizeMode(0, QHeaderView::Stretch);
	tableView->horizontalHeader()->setStretchLastSection(false);
	this->tableView->setModel(modelePlug_in);
	this->tableView->setColumnWidth(0,30);
	this->tableView->setColumnWidth(1,130);
	this->tableView->setColumnWidth(2,30);
	this->tableView->setColumnWidth(3,330);
 	this->Connecter->setEnabled(true);

	//this->
	}
else this->lineEdit->setText("Connecter un module USBTin");
}

void USBTin::onMCP2515(){
	mreg->setWindowModality(Qt::ApplicationModal);
	mreg->show();
	//mcan->getRegistre(0x20);
}
/**
 * Destructeur
 */
USBTin::~USBTin()
{
	//qDebug()<<"fin programme";

	if (openport){
		delete mcan;
            delete mreg;
        }

}

void USBTin::onDeconnectS(){
    if (opencan)
            mcan->Fermer();
    opencan=false;
    delete mcan;
}

void USBTin::onConnecter(){
 QString port=this->PortCom->currentText();
 if (!openport){
	mcan=new BusCAN(this);
	mreg=new RegMCP2515Dlg(mcan,this);
	connect(mcan,SIGNAL(pourLire()),this,SLOT(onRecu()));
 	//connect(mcan,SIGNAL(confirm()),this,SLOT(onConfirm()));
 		connect(mcan,SIGNAL(ouvertureCAN()),this,SLOT(slotOpenCAN()));
 		connect(mcan,SIGNAL(fermetureCAN()),this,SLOT(slotCloseCAN()));
 		connect(mcan,SIGNAL(vitesseCAN()),this,SLOT(slotVitesseCAN()));
 		connect(mcan,SIGNAL(lireReg(unsigned char)),this,SLOT(slotLireMCP2515(unsigned char)));
	openport=true;
    if (mcan->openCAN(&port)){
	   this->lineEdit->setText("open");
	   this->Vitesse->setEnabled(true);
	   this->Effacer->setEnabled(true);
	   this->Sauvegarder->setEnabled(true);
	   this->MCP2515->setEnabled(true);
           this->Envoie->setEnabled(true);
           this->Ouvrir->setEnabled(true);
          // this->Deconnect->setEnabled(true);

  }
  else	this->lineEdit->setText("close");
  }
 mcan->setVitesse(250);
}


void USBTin::onCheckExtended(){
	if (this->checkextended){
		this->checkextended=false;
		mode=STD;
	}
	else {
		this->checkextended=true;
		mode=EXT;
	}
}

void USBTin::onRTR(){
	if (this->checkrtr)
		this->checkrtr=false;
	else this->checkrtr=true;
}



void USBTin::onDeconnect(){
	if (opencan)
		mcan->Fermer();
	opencan=false;
}

void USBTin::onOuvrir(){
	 // QString vitesse=this->Vitesse->currentText();
	 // int vit=vitesse.toInt();
	  mcan->ouvrirCAN();
	  //qDebug()<<mcan->getConfirm();
	  opencan=true;
	  this->Envoie->setEnabled(true);
          this->Deconnect->setEnabled(true);
          this->Ouvrir->setEnabled(false);

}

void USBTin::onVitesse(int index){
	int vitesse=1000;
	switch (index){
	case 0:vitesse=1000;break;
	case 1:vitesse=250;break;
	case 2:vitesse=125;break;
	case 3:vitesse=500;break;
	}
	//qDebug()<<"vitesse"<<vitesse;
	if (openport)
		mcan->setVitesse(vitesse);
}


void USBTin::onEnvoie(){
	int i;
	bool messagelu=true;
	QString strid;
	QByteArray bytearid;
	TMessage messagecan;
	if (this->checkextended)
		messagecan.typem=EXTENDED;
	else	messagecan.typem=STANDART;
	int identifiant=this->Identifiant->text().toInt(&messagelu,16);
	messagecan.id=identifiant;
	bytearid.setNum(identifiant,16);
	messagecan.length=this->DLC->value();
	messagecan.donnee[0]=this->D0->text().toLatin1();
	messagecan.donnee[1]=this->D1->text().toLatin1();
	messagecan.donnee[2]=this->D2->text().toLatin1();
	messagecan.donnee[3]=this->D3->text().toLatin1();
	messagecan.donnee[4]=this->D4->text().toLatin1();
	messagecan.donnee[5]=this->D5->text().toLatin1();
	messagecan.donnee[6]=this->D6->text().toLatin1();
	messagecan.donnee[7]=this->D7->text().toLatin1();
	// test si les cases correspondantes sont remplies D0 à D7
	for(i=0;i<messagecan.length;i++){
		if (!messagecan.donnee[i].length()>0)
			messagelu=false;
	}
	if (messagelu){
		mcan->EnvoyerMessage(&messagecan);
	}
}

void USBTin::onAcceptanceMask(){
	bool messagelu=true;
	QString strid;
	QByteArray bytemask;
	unsigned int mask=this->LineAcceptance->text().toUInt(&messagelu,16);
	bytemask.setNum(mask,16);
	if (messagelu)
		//mcan->setAcceptance(mask,MASK);
		mcan->setAcceptanceReg(mask,MASK,mode);
}

void USBTin::onAcceptanceFiltre(){
	bool messagelu=true;
	QByteArray bytemask;
	unsigned int filtre=this->lineAcceptanceFiltre->text().toUInt(&messagelu,16);
	//bytemask.setNum(mask,16);
	if (messagelu)
		//mcan->setAcceptance(filtre,FILTRE);
		mcan->setAcceptanceReg(filtre,FILTRE,mode);

}

void USBTin::onRecu(){

	TMessage mess=mcan->getMessageRecu();
	//qDebug()<<"Message recu:"<<mess;
	QStandardItem* itm;
	QString idstr;
	QString lengthstr;
	QString datastr;
	idstr.setNum(mess.id,16);
	lengthstr.setNum(mess.length);
	if (mess.typem==STANDART)
		itm = new QStandardItem(QString("std"));
	else itm = new QStandardItem(QString("ext"));
	modelePlug_in->appendRow(itm);
	modelePlug_in->setItem(numeromessage, 1, new QStandardItem(idstr));
	modelePlug_in->setItem(numeromessage, 2, new QStandardItem(lengthstr));
	for(int i=0;i<mess.length;i++){
		datastr+=mess.donnee[i];
		datastr+=" ";
	}
	modelePlug_in->setItem(numeromessage, 3, new QStandardItem(datastr));

	numeromessage++;
}

void USBTin::onEffacer(){
	modelePlug_in->clear();
	numeromessage=0;
}

void USBTin::onConfirm(){
	//this->lineEdit->setText("confirm");
}

void USBTin::onSauvegarder(){

}

void USBTin::slotOpenCAN(){
 	this->Ouvrir->setEnabled(false);
  	this->Deconnect->setEnabled(true);

}

void USBTin::slotCloseCAN(){
 	this->Ouvrir->setEnabled(true);
  	this->Deconnect->setEnabled(false);

}

void USBTin::slotVitesseCAN(){
 	this->Ouvrir->setEnabled(true);
  	this->Deconnect->setEnabled(false);
}

void USBTin::slotLireMCP2515(unsigned char reg){
	  	mcp->setRegistre(reg);
	  	mreg->AfficheReg(reg);

}
