/*
 * BusCAN.cpp
 *
 *  Created on: 20 déc. 2015
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         BusCAN.cpp
 *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier BusCAN.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */
#include "BusCAN.h"
#include <stdio.h>
#include <QDebug>
//SerialPort::SerialPort(QObject *parent) :
//QSerialPort(parent) {
#define RXM0 0x20
#define RXM1 0x24
#define RXF0 0x00
#define RXF1 0x04
#define RXF2 0x08
#define RXF3 0x10
#define RXF4 0x14
#define RXF5 0x18

BusCAN::BusCAN(QObject *parent): QObject(parent)
//BusCAN::BusCAN()
{
	// TODO Auto-generated constructor stub
	mport=new SerialPort();
	mbat=new Battement(this);
 	connect(mport,SIGNAL(pourLireCAN()),this,SLOT(getMessageCAN()));
 	connect(mport,SIGNAL(confirm()),this,SLOT(slotConfirm()));
 	//connect(this,SIGNAL(readConfirm()),this,SLOT(slotConfirm()));
	typecan=STANDART;
	mopencan=FERME;

	open=false;
}


BusCAN::~BusCAN() {
	// TODO Auto-generated destructor stub
	// qDebug()<<"lancement fin port";
    if (mport->isOpen()){
    	 mport->write("C\015");
    	// qDebug()<<" port en fermeture";
    	 delete mport;
    	 //qDebug()<<"lancement port fermé";
     }


}

void BusCAN::Fermer(){
    if (mport->isOpen()){
	    mport->flush();
    	mopencan=FERMETURE;
    	mport->write("C\015");
        mport->waitForBytesWritten(-1);
    }
}

bool BusCAN::openCAN( QString *dev){
	mdev=dev;
	open=mport->setOpenPort(mdev);
	return open;
}

void BusCAN::ouvrirCAN(){
	int n;
	  if (mport->isOpen()){
		  mopencan=OUVERTURE;
			    mport->flush();
			    mport->write("O\015");
                            mport->waitForBytesWritten(-1);
			    //open=true;
	  }
}

void BusCAN::setVitesse(int vitesse){
	char	Message[30];int n;
	switch (vitesse){
		case 10:	sprintf(Message,"S0\015");	break;
		case 20:	sprintf(Message,"S1\015");	break;
		case 50:	sprintf(Message,"S2\015");	break;
		case 100:	sprintf(Message,"S3\015");	break;
		case 125:	sprintf(Message,"S4\015");	break;
		case 250:	sprintf(Message,"S5\015");	break;
		case 500:	sprintf(Message,"S6\015");	break;
		case 800:	sprintf(Message,"S7\015");	break;
		case 1000:	sprintf(Message,"S8\015");	break;

	}
	  if (mport->isOpen()){
		  mopencan=VITESSE;
		  mport->flush();
		  mport->write(Message);
		 // mport->writeBuf("S5\015",3);
		  mport->waitForBytesWritten(100);
		 // mport->waitForReadyRead(150);
	    	//n=mport->bytesAvailable();
		 // int a=mport->getNextByte();
		 // qDebug()<<"n="<<n<<"a="<<a<<(int)(Message[0]);
	  }

}

char inttoASCII(unsigned int a, int n){
	char retour;
	unsigned int masq=0xF<<(n*4);
	unsigned int val=(a&masq)>>(n*4);
	if (val<10)
		retour=(char)(val+'0');
		else
			retour=(char)((val-10)+'A');
	return retour;
}

/*
 * Envoie d'un message can
 */
bool BusCAN::EnvoyerMessage(TMessage *mess){
	char message[50]={'t'};
	int id=mess->id;
	int i,indice,idlength;;
	QByteArray bytearid;
	bytearid.setNum(id,16);
	if (mess->typem==STANDART){
		idlength=IDLENGTHSTD;
	}
	else {
		idlength=IDLENGTHEXT;
		message[0]='T';
	}
	while (bytearid.length()<idlength)
			bytearid.insert(0,'0');
	for(indice=0;indice<idlength;indice++){
			message[indice+1]=(char)(bytearid[indice]);
	}
	indice++;
	message[indice++]=mess->length+0x30;
	for(i=0;i<mess->length;i++){
		if (mess->donnee[i].length()>1){
			message[indice++]=mess->donnee[i].at(0);
			message[indice++]=mess->donnee[i].at(1);
		}
		else {
			message[indice++]=0x30;
			message[indice++]=mess->donnee[i].at(0);
		}
	}
	message[indice++]='\015';
	message[indice]='\0';
	mport->write(message);
}


bool BusCAN::setAcceptance(unsigned int val,int type){
	char message[50]={'M'};int i;
	for(i=0;i<8;i++){
		message[i+1]=inttoASCII(val,7-i);
	}
	i++;
	message[i++]='\015';
	message[i]='\0';
	if (type==FILTRE)
		message[0]='M';
	else message[0]='m';
	//qDebug()<<message;

	mport->flush();
	mport->write(message);
	mport->waitForBytesWritten(100);
	mport->flush();
}

void BusCAN::setAcceptanceReg(unsigned int val,int type,int mode){
	unsigned char tabval[4];
	int i;
	if (type==FILTRE){
		if (mode==EXT){
			tabval[3]=(unsigned char)(val);
			tabval[2]=(unsigned char)(val>>8);
			tabval[0]=(unsigned char)(val>>21);
			tabval[1]=0x8;
			tabval[1]+=(unsigned char)((val>>16)&0x3);
			tabval[1]+=(unsigned char)((val>>13)&0xE0);
		}
			else{
				tabval[0]=(unsigned char)(val>>3);
				tabval[1]=(unsigned char)((val<<5)&0xE0);
			}
			for(i=0;i<4;i++){
				this->ecrireReg(RXF0+i,tabval[i]);
				this->ecrireReg(RXF1+i,tabval[i]);
				this->ecrireReg(RXF2+i,tabval[i]);
				this->ecrireReg(RXF3+i,tabval[i]);
				this->ecrireReg(RXF4+i,tabval[i]);
				this->ecrireReg(RXF5+i,tabval[i]);
			}
	}
	else{
		if (mode==EXT){

		tabval[3]=(unsigned char)(val);
		tabval[2]=(unsigned char)(val>>8);
		tabval[0]=(unsigned char)(val>>21);
		tabval[1]=(unsigned char)((val)&0xE0)+(unsigned char)((val>>16)&0x3);
		}
		else{
			tabval[0]=(unsigned char)(val>>3);
			tabval[1]=(unsigned char)((val<<5)&0xE0);
		}
		for(i=0;i<4;i++){
			this->ecrireReg(RXM0+i,tabval[i]);
			this->ecrireReg(RXM1+i,tabval[i]);
		}
	}
}

void BusCAN::getMessageCAN(){
	char messageretour[50];
//	bool messr=true;
	int idlength;
	int i=0;
	int id=0;
	int l=0;
	int j=0;
	mport->getMessagetoCAN(messageretour);
	switch (messageretour[0]){
	case 't':	messagelu.typem=STANDART;break;
	case 'T':	messagelu.typem=EXTENDED;break;
	//default :	//messr=false;
	}

		   if (messagelu.typem==STANDART){
				idlength=IDLENGTHSTD;
			}
			else  idlength=IDLENGTHEXT;

			   while ((messageretour[i]!='\015')&&(i<=idlength)){
				   if ((messageretour[i]>=0x30)&&(messageretour[i]<='9')){
					   id = id*16 + messageretour[i]-0x30;
				   }
				   if ((messageretour[i]>='A')&&(messageretour[i]<='F')){
						id = id*16 + messageretour[i]-0x37;
					}
				   i++;
			   }

		messagelu.id=id;
		messagelu.length=messageretour[i++]-0x30;
		while ((messageretour[i]!='\015')&&(l<=8)){
			messagelu.donnee[l][j]
					=messageretour[i];
			i++;j++;
			if (j>=2){
				j=0;l++;
			}
		}
		monmessage.push_back(messagelu);
		emit pourLire();
}

unsigned char tabtoUchar(char * message){
	int i;unsigned char val;
	unsigned char mul=16;
	unsigned valretour=0;
	for(i=0;i<2;i++){
		if ((message[i])<0x40){
			val=message[i]-0x30;
		}else val=message[i]-0x41+10;
		valretour+=val*mul;
		mul=mul/16;
	}
	return valretour;
}

void BusCAN::slotConfirm(){
	char message[50];
	mport->getMessagetoCAN(message);
	//message[0]=mport->getNextByte();
	switch (mopencan){
	case OUVERTURE:if (message[0]==13){
						mopencan=OUVERT;
						emit (ouvertureCAN());
	}
					break;
	case FERMETURE:if (message[0]==13){
						mopencan=FERME;
						emit (fermetureCAN());
	}
					break;
	case VITESSE:if (message[0]==13){
						mopencan=FERME;
						emit (vitesseCAN());
	}
					break;
	case LIREREG:if (message[2]==13){
						mopencan=FERME;
						int val=tabtoUchar(message);
						//qDebug()<<"lirereg"<<val;
						emit (lireReg(val));
	}
					break;

	}
	//qDebug()<<"slotconfirm"<<(int)(message[0]);
	if (message[0]==13){
		this->confirmation=true;
		//qDebug()<<"confirm";
	}

}

bool BusCAN::getConfirm(){
	return this->confirmation;
}


TMessage BusCAN::getMessageRecu(){
	return messagelu;
}



QVector<Message> BusCAN::getMessagesCAN(){
	return monmessage;
}



unsigned char BusCAN::getRegistre(unsigned char reg){
	char message[50]={'G'};
	message[1]=inttoASCII(reg,1);
	message[2]=inttoASCII(reg,0);
	message[3]='\015';
	message[4]='\0';
	mopencan=LIREREG;
	confirmation=false;
	mbat->raztemps();
	mport->flush();
	mport->writeBuf(message,4);
	mport->flush();

}




void BusCAN::ecrireReg(unsigned char reg,unsigned char val){
	char message[50]={'M'};
	mport->flush();
	message[0]='W';
	message[1]=inttoASCII(reg,1);
	message[2]=inttoASCII(reg,0);
	message[3]=inttoASCII(val,1);
	message[4]=inttoASCII(val,0);
	message[5]='\015';
	message[6]='\0';
	mport->write(message);
	mport->flush();

}
