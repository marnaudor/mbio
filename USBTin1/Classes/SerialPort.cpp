/*
 * SerialPort.cpp
 *
 *  Created on: 9 févr. 2015
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         SerialPort.cpp
 *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier SerialPort.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */
#include "SerialPort.h"
//#include <QDebug>

SerialPort::SerialPort(QObject *parent) :
QSerialPort(parent) {
 	connect(this,SIGNAL(readyRead()),this,SLOT(RecevoirMessage()));
	messageretour= new char[50];

}

bool SerialPort::setOpenPort(QString *port){
	this->setPortName(*port);
	this->close();
	this->setBaudRate(115200);
	if (!this->isOpen())
		openport=this->open(QIODevice::ReadWrite);
	else openport=true;
	this->setBaudRate(115200);
	return openport;
}

SerialPort::~SerialPort() {
	// TODO Auto-generated destructor stub
	delete []messageretour;
	this->flush();
	this->close();
}

unsigned char SerialPort::getNextByte(){
	char data;
 this->readData(&data,1);
 return (unsigned char)(data);
}


int SerialPort::writeBuf(char *buf,int l){
	int j=this->writeData(buf,l);
	return j;
}


bool SerialPort::getOpen(){
	return openport;
}

void SerialPort::setRazMessage(){
	messageretour[0]=0;
}

char SerialPort::getMessage0(){
	return messageretour[0];
}

void SerialPort::getMessagetoCAN(char *message){
	for(int i=0;i<nblue;i++){
		message[i]=messageretour[i];
	}
}

void SerialPort::RecevoirMessage(){
	nblue=this->read(messageretour,50);
	//qDebug()<<"Message recu:"<<nblue<<": "<<((int)(messageretour[0]))<<": "<<((int)(messageretour[1]))
//			<<": "<<((int)(messageretour[2]))<<": "<<((int)(messageretour[3]));
   if ((messageretour[0]=='t')||(messageretour[0]=='T')){
	this->flush();
	emit (pourLireCAN());
	}
	else {
		//qDebug()<<"confirmation";//<<mbat->liretemps();
		this->flush();
		emit confirm();
	}

}


