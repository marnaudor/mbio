#ifndef USBTIN_H
#define USBTIN_H

#include <QMainWindow>
//#include "RegMCP2515Dlg.h"
#include "BusCAN.h"
//#include "MCP2515.h"
#include <QStandardItemModel>
#include "device.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class Usbtin;
}
QT_END_NAMESPACE

class Usbtin : public QMainWindow
{
    Q_OBJECT
 //   RegMCP2515Dlg *mreg;
    SerialPort *mport;
   // MCP2515Reg *mcp;
    int numeromessage;
    BusCAN *mcan;
    bool opencan;
    bool openport;
    QStandardItemModel *modelePlug_in;
    bool checkextended;
    bool checkrtr;
    Device *mondevice;
    int mode;
    TMessage messagecan;
public:
    Usbtin(QWidget *parent = nullptr);
    ~Usbtin();

private slots:
    void onRecu();
    void slotOpenCAN();
    void slotCloseCAN();
    void slotVitesseCAN();
    void onConfirm();
    void slotLireMCP2515(unsigned char reg);

    void on_Connecter_clicked();

    void on_Deconnecter_clicked();

    void on_Vitesse_activated(int index);

    void on_Sauvegarder_clicked();

    void on_Effacer_clicked();

    void on_AcceptanceMask_clicked();

    void on_AcceptanceFiltre_clicked();

    void on_MCP2515_clicked();

    void on_OuvrirCAN_clicked();

    void on_FermerCAN_clicked();

    void on_Envoie_clicked();

    void on_EnvoieData_clicked();

    void on_Extended_stateChanged(int arg1);

    void on_Rtr_stateChanged(int arg1);

private:
    Ui::Usbtin *ui;
};
#endif // USBTIN_H
