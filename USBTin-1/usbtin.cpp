#include "usbtin.h"
#include "ui_usbtin.h"

Usbtin::Usbtin(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Usbtin)
{
    ui->setupUi(this);
    ui->Connecter->setEnabled(false);
    ui->Deconnecter->setEnabled(false);
    ui->Effacer->setEnabled(false);
    ui->Envoie->setEnabled(false);
    ui->OuvrirCAN->setEnabled(false);
    ui->Sauvegarder->setEnabled(false);
    ui->Vitesse->setEnabled(false);
    ui->FermerCAN->setEnabled(false);
    ui->MCP2515->setEnabled(false);
    ui->AcceptanceFiltre->setEnabled(false);
    ui->AcceptanceMask->setEnabled(false);
    int i;
    numeromessage=0;
    mode=STD;
    //	char val;
    opencan=false;
    openport=false;
    checkextended=false;
    checkrtr=false;
    QVector<QString> dev;
    mondevice=new Device();
    bool devtrouve=mondevice->getDevices(&dev);

    if (devtrouve){
        for(i=0;i<dev.length();i++)
            ui->PortCom->addItem(dev[i]);

        ui->Vitesse->addItem("1000k");
        ui->Vitesse->addItem("250k");
        ui->Vitesse->addItem("125k");
        ui->Vitesse->addItem("100k");


        QStringList listeHeader;
        listeHeader << "s/e" << "identifiant"<<"l"<<"Data";
        QStringListModel *modeleHeader = new QStringListModel(listeHeader);
        QHeaderView *vueHeader = new QHeaderView(Qt::Horizontal);
        vueHeader->setModel(modeleHeader);
        ui->tableView->setHorizontalHeader(vueHeader);
        //vuePlug_in->setHorizontalHeader(vueHeader);
        //vuePlug_in->setGeometry(0, 80, 400, 200);
        modelePlug_in = new QStandardItemModel(0, 4);
        modelePlug_in->setHorizontalHeaderLabels(listeHeader);
        ui->tableView->horizontalHeader()->setStretchLastSection(false);
        ui->tableView->setModel(modelePlug_in);
        ui->tableView->setColumnWidth(0,30);
        ui->tableView->setColumnWidth(1,130);
        ui->tableView->setColumnWidth(2,30);
        ui->tableView->setColumnWidth(3,330);

        ui->Connecter->setEnabled(true);

        //this->
    }
    else ui->lineEdit->setText("Connecter un module USBTin");


}

Usbtin::~Usbtin()
{    if (openport){
        delete mcan;
//       delete mreg;
    }
    delete ui;
}

void Usbtin::on_Connecter_clicked()
{
    QString port=ui->PortCom->currentText();
    if (!openport){
        mcan=new BusCAN(this);
         connect(mcan,SIGNAL(pourLire()),this,SLOT(onRecu()));
          connect(mcan,SIGNAL(ouvertureCAN()),this,SLOT(slotOpenCAN()));
          connect(mcan,SIGNAL(fermetureCAN()),this,SLOT(slotCloseCAN()));
          connect(mcan,SIGNAL(vitesseCAN()),this,SLOT(slotVitesseCAN()));
          connect(mcan,SIGNAL(lireReg(unsigned char)),this,SLOT(slotLireMCP2515(unsigned char)));



        // mreg=new RegMCP2515Dlg(mcan,this);
         openport=true;
        if (mcan->openCAN(&port)){
            ui->lineEdit->setText("open");
            ui->Vitesse->setEnabled(true);
            ui->Effacer->setEnabled(true);
            ui->Sauvegarder->setEnabled(true);
            ui->Deconnecter->setEnabled(true);
          //  ui->MCP2515->setEnabled(true);
          //   ui->Envoie->setEnabled(true);
            ui->OuvrirCAN->setEnabled(true);
            // this->Deconnect->setEnabled(true);

        }
        else	ui->lineEdit->setText("close");
    }
    mcan->setVitesse(250);
}

void Usbtin::onConfirm(){
    //this->lineEdit->setText("confirm");
}

void Usbtin::slotOpenCAN(){
    ui->OuvrirCAN->setEnabled(false);
    ui->FermerCAN->setEnabled(true);

}

void Usbtin::slotCloseCAN(){
    ui->OuvrirCAN->setEnabled(true);
    ui->FermerCAN->setEnabled(false);

}

void Usbtin::slotVitesseCAN(){
    ui->OuvrirCAN->setEnabled(true);
    ui->FermerCAN->setEnabled(false);
}

void Usbtin::slotLireMCP2515(unsigned char reg){
 //   mcp->setRegistre(reg);
 //   mreg->AfficheReg(reg);

}



void Usbtin::on_Deconnecter_clicked()
{
    if (opencan){
        mcan->Fermer();
        ui->Vitesse->setEnabled(false);
        ui->Connecter->setEnabled(true);
        //  ui->MCP2515->setEnabled(true);
        //   ui->Envoie->setEnabled(true);
        ui->OuvrirCAN->setEnabled(false);
        ui->FermerCAN->setEnabled(false);

    }
    opencan=false;
    delete mcan;
}


void Usbtin::on_Vitesse_activated(int index)
{
    int vitesse=1000;
    switch (index){
    case 0:vitesse=1000;break;
    case 1:vitesse=250;break;
    case 2:vitesse=125;break;
    case 3:vitesse=100;break;
    }
    //qDebug()<<"vitesse"<<vitesse;
    if (openport)
        mcan->setVitesse(vitesse);
}

void Usbtin::onRecu(){

    TMessage mess=mcan->getMessageRecu();
    //qDebug()<<"Message recu:"<<mess;
    QStandardItem* itm;
    QString idstr;
    QString lengthstr;
    QString datastr;
    idstr.setNum(mess.id,16);
    lengthstr.setNum(mess.length);
    if (mess.typem==STANDART)
        itm = new QStandardItem(QString("std"));
    else itm = new QStandardItem(QString("ext"));
    modelePlug_in->appendRow(itm);
    modelePlug_in->setItem(numeromessage, 1, new QStandardItem(idstr));
    modelePlug_in->setItem(numeromessage, 2, new QStandardItem(lengthstr));
    for(int i=0;i<mess.length;i++){
        datastr+=mess.donnee[i];
        datastr+=" ";
    }
    modelePlug_in->setItem(numeromessage, 3, new QStandardItem(datastr));

    numeromessage++;
}

void Usbtin::on_Sauvegarder_clicked()
{

}


void Usbtin::on_Effacer_clicked()
{
    modelePlug_in->clear();
    numeromessage=0;
}


void Usbtin::on_AcceptanceMask_clicked()
{
    bool messagelu=true;
    QString strid;
    QByteArray bytemask;
    unsigned int mask=ui->LineAcceptance->text().toUInt(&messagelu,16);
    bytemask.setNum(mask,16);
    if (messagelu)
        //mcan->setAcceptance(mask,MASK);
        mcan->setAcceptanceReg(mask,MASK,mode);
}


void Usbtin::on_AcceptanceFiltre_clicked()
{
    bool messagelu=true;
    QByteArray bytemask;
    unsigned int filtre=ui->lineAcceptanceFiltre->text().toUInt(&messagelu,16);
    //bytemask.setNum(mask,16);
    if (messagelu)
        //mcan->setAcceptance(filtre,FILTRE);
        mcan->setAcceptanceReg(filtre,FILTRE,mode);

}


void Usbtin::on_MCP2515_clicked()
{

}


void Usbtin::on_OuvrirCAN_clicked()
{
    mcan->ouvrirCAN();
    //qDebug()<<mcan->getConfirm();
    opencan=true;
    ui->Envoie->setEnabled(true);
    ui->FermerCAN->setEnabled(true);
    ui->OuvrirCAN->setEnabled(false);
}


void Usbtin::on_FermerCAN_clicked()
{
    if (opencan)
        mcan->Fermer();
    opencan=false;
    ui->Envoie->setEnabled(false);
    ui->FermerCAN->setEnabled(false);
    ui->OuvrirCAN->setEnabled(true);
}


void Usbtin::on_Envoie_clicked()
{
    int i;
    bool messagelu=true;
    QString strid;
    QByteArray bytearid;

    if (this->checkextended)
        messagecan.typem=EXTENDED;
    else	messagecan.typem=STANDART;
    int identifiant=ui->Identifiant->text().toInt(&messagelu,16);
    messagecan.id=identifiant;
    bytearid.setNum(identifiant,16);
    messagecan.length=ui->DLC->value();
    messagecan.donnee[0]=ui->D0->text().toLatin1();
    messagecan.donnee[1]=ui->D1->text().toLatin1();
    messagecan.donnee[2]=ui->D2->text().toLatin1();
    messagecan.donnee[3]=ui->D3->text().toLatin1();
    messagecan.donnee[4]=ui->D4->text().toLatin1();
    messagecan.donnee[5]=ui->D5->text().toLatin1();
    messagecan.donnee[6]=ui->D6->text().toLatin1();
    messagecan.donnee[7]=ui->D7->text().toLatin1();
    // test si les cases correspondantes sont remplies D0 à D7
    for(i=0;i<messagecan.length;i++){
        if (!messagecan.donnee[i].length()>0)
            messagelu=false;
    }
 //   mcan->EnvoyerM();
    if (messagelu){
        mcan->EnvoyerMessage(messagecan);
    // mcan->ouvrirCAN();
    }
}


void Usbtin::on_EnvoieData_clicked()
{
 //mcan->EnvoyerMessage(&messagecan);
}


void Usbtin::on_Extended_stateChanged(int arg1)
{
    if (this->checkextended){
        this->checkextended=false;
        mode=STD;
    }
    else {
        this->checkextended=true;
        mode=EXT;
    }
}


void Usbtin::on_Rtr_stateChanged(int arg1)
{
    if (this->checkrtr)
        this->checkrtr=false;
    else this->checkrtr=true;
}

