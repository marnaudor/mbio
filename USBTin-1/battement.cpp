/*
 * battement.cpp
 *
 *  Created on: 20 déc. 2015
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         battement.cpp
 *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier battement.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */



#include "battement.h"
#include <iostream>

using namespace std;


Battement::Battement(QObject *parent) :
    	    QThread(parent)
/*: QThread() */{
     stopped=false;
     start();
}

void Battement::run(){


    while(stopped==false){
    	this->msleep(10);
//        this->usleep(20);
//      emit newPeriode(1000);
        t++;
    }
   stopped=false;
 }

 void Battement::startb(){
     start();
     stopped=false;
 }

void Battement::raztemps(){
	t=0;
}

int Battement::liretemps(){
	return t;
}

 void Battement::stop(){
    stopped=true;
 }
