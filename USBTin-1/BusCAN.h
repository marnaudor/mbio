/*
 * BusCAN.h
 *
 *  Created on: 20 déc. 2015
 *      Author: michel
 */

#ifndef BUSCAN_H_
#define BUSCAN_H_
#include "SerialPort.h"
//#include "MCP2515.h"
#include <QObject>
#include <QtCore>
#include "battement.h"
#include <QVector>
#define FILTRE 1
#define MASK 2
#define EXT 3
#define STD 4
enum type {STANDART,EXTENDED};
enum opencan{OUVERTURE,FERMETURE,OUVERT,FERME,VITESSE,LIREREG};



typedef struct TMessage{
	enum type typem;
	int id;
	int length;
    QByteArray donnee[8];
}Message;




class BusCAN: public QObject
{
	 Q_OBJECT
    // Battement *mbat;
	SerialPort *mport;
	QString *mdev;
	TMessage messagelu;
	int mavitesse;
	bool open;
	enum opencan mopencan;
	enum type typecan;
	bool confirmation;
	QVector<Message> monmessage;

public:
	BusCAN(QObject *parent = 0);
	//BusCAN();
	virtual ~BusCAN();
	bool openCAN( QString *dev);
    void EnvoyerMessage(TMessage &mess);
	void setVitesse(int vit);
	TMessage getMessageRecu();
	void Fermer();
	void ouvrirCAN();
	bool setAcceptance(unsigned int val,int type);
	void setAcceptanceReg(unsigned int val,int type,int mode);
   	bool getConfirm();
    QVector<Message> getMessagesCAN();
	unsigned char getRegistre(unsigned char reg);
    void EnvoyerM();
	void ecrireReg(unsigned char reg,unsigned char val);


    public slots:
		void slotConfirm();
    	void getMessageCAN();
    signals:
    	void pourLire();
    	void readConfirm();
    	void ouvertureCAN();
    	void fermetureCAN();
    	void vitesseCAN();
    	void lireReg(unsigned char);
 //   	void pourConfirm

};

#endif /* BUSCAN_H_ */
