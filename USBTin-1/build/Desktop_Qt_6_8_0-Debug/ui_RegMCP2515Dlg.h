/********************************************************************************
** Form generated from reading UI file 'RegMCP2515Dlg.ui'
**
** Created by: Qt User Interface Compiler version 6.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGMCP2515DLG_H
#define UI_REGMCP2515DLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegMCP2515Dlg
{
public:
    QWidget *centralwidget;
    QPushButton *Lire;
    QComboBox *Registre;
    QLineEdit *lineEdit;
    QLCDNumber *lcdNumber;
    QPushButton *Ecrire;
    QComboBox *Registreext;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *RegMCP2515Dlg)
    {
        if (RegMCP2515Dlg->objectName().isEmpty())
            RegMCP2515Dlg->setObjectName("RegMCP2515Dlg");
        RegMCP2515Dlg->resize(501, 211);
        centralwidget = new QWidget(RegMCP2515Dlg);
        centralwidget->setObjectName("centralwidget");
        Lire = new QPushButton(centralwidget);
        Lire->setObjectName("Lire");
        Lire->setGeometry(QRect(60, 20, 98, 27));
        Registre = new QComboBox(centralwidget);
        Registre->setObjectName("Registre");
        Registre->setGeometry(QRect(380, 20, 78, 27));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName("lineEdit");
        lineEdit->setGeometry(QRect(50, 90, 113, 27));
        lcdNumber = new QLCDNumber(centralwidget);
        lcdNumber->setObjectName("lcdNumber");
        lcdNumber->setGeometry(QRect(170, 20, 64, 23));
        Ecrire = new QPushButton(centralwidget);
        Ecrire->setObjectName("Ecrire");
        Ecrire->setGeometry(QRect(60, 120, 98, 27));
        Registreext = new QComboBox(centralwidget);
        Registreext->setObjectName("Registreext");
        Registreext->setGeometry(QRect(380, 90, 79, 23));
        RegMCP2515Dlg->setCentralWidget(centralwidget);
        menubar = new QMenuBar(RegMCP2515Dlg);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 501, 20));
        RegMCP2515Dlg->setMenuBar(menubar);
        statusbar = new QStatusBar(RegMCP2515Dlg);
        statusbar->setObjectName("statusbar");
        RegMCP2515Dlg->setStatusBar(statusbar);

        retranslateUi(RegMCP2515Dlg);

        QMetaObject::connectSlotsByName(RegMCP2515Dlg);
    } // setupUi

    void retranslateUi(QMainWindow *RegMCP2515Dlg)
    {
        RegMCP2515Dlg->setWindowTitle(QCoreApplication::translate("RegMCP2515Dlg", "MainWindow", nullptr));
        Lire->setText(QCoreApplication::translate("RegMCP2515Dlg", "Lire Registre", nullptr));
        Ecrire->setText(QCoreApplication::translate("RegMCP2515Dlg", "Ecrire Registre", nullptr));
    } // retranslateUi

};

namespace Ui {
    class RegMCP2515Dlg: public Ui_RegMCP2515Dlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGMCP2515DLG_H
