/********************************************************************************
** Form generated from reading UI file 'usbtin.ui'
**
** Created by: Qt User Interface Compiler version 6.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USBTIN_H
#define UI_USBTIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Usbtin
{
public:
    QWidget *centralwidget;
    QLineEdit *lineAcceptanceFiltre;
    QPushButton *AcceptanceFiltre;
    QLabel *label_2;
    QLabel *label_8;
    QLineEdit *D2;
    QCheckBox *Rtr;
    QLineEdit *D1;
    QLineEdit *lineEdit;
    QLineEdit *D4;
    QLineEdit *D3;
    QComboBox *Vitesse;
    QLabel *label;
    QPushButton *Connecter;
    QLabel *label_4;
    QTableView *tableView;
    QPushButton *Effacer;
    QLabel *label_7;
    QPushButton *OuvrirCAN;
    QPushButton *FermerCAN;
    QLabel *label_9;
    QSpinBox *DLC;
    QLabel *label_3;
    QLabel *label_10;
    QPushButton *Deconnecter;
    QLabel *label_5;
    QComboBox *PortCom;
    QLineEdit *D5;
    QLineEdit *Identifiant;
    QLineEdit *D6;
    QLineEdit *D0;
    QCheckBox *Extended;
    QPushButton *MCP2515;
    QPushButton *Envoie;
    QPushButton *AcceptanceMask;
    QLineEdit *LineAcceptance;
    QPushButton *Sauvegarder;
    QLineEdit *D7;
    QLabel *label_6;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Usbtin)
    {
        if (Usbtin->objectName().isEmpty())
            Usbtin->setObjectName("Usbtin");
        Usbtin->resize(823, 709);
        centralwidget = new QWidget(Usbtin);
        centralwidget->setObjectName("centralwidget");
        lineAcceptanceFiltre = new QLineEdit(centralwidget);
        lineAcceptanceFiltre->setObjectName("lineAcceptanceFiltre");
        lineAcceptanceFiltre->setGeometry(QRect(520, 630, 113, 23));
        AcceptanceFiltre = new QPushButton(centralwidget);
        AcceptanceFiltre->setObjectName("AcceptanceFiltre");
        AcceptanceFiltre->setGeometry(QRect(340, 630, 141, 23));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(150, 450, 41, 20));
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName("label_8");
        label_8->setGeometry(QRect(480, 450, 21, 17));
        D2 = new QLineEdit(centralwidget);
        D2->setObjectName("D2");
        D2->setGeometry(QRect(320, 470, 41, 27));
        Rtr = new QCheckBox(centralwidget);
        Rtr->setObjectName("Rtr");
        Rtr->setGeometry(QRect(130, 500, 97, 22));
        D1 = new QLineEdit(centralwidget);
        D1->setObjectName("D1");
        D1->setGeometry(QRect(270, 470, 41, 27));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName("lineEdit");
        lineEdit->setGeometry(QRect(30, 550, 681, 61));
        D4 = new QLineEdit(centralwidget);
        D4->setObjectName("D4");
        D4->setGeometry(QRect(420, 470, 41, 27));
        D3 = new QLineEdit(centralwidget);
        D3->setObjectName("D3");
        D3->setGeometry(QRect(370, 470, 41, 27));
        Vitesse = new QComboBox(centralwidget);
        Vitesse->setObjectName("Vitesse");
        Vitesse->setGeometry(QRect(260, 30, 101, 27));
        Vitesse->setAutoFillBackground(false);
        label = new QLabel(centralwidget);
        label->setObjectName("label");
        label->setGeometry(QRect(20, 450, 91, 17));
        Connecter = new QPushButton(centralwidget);
        Connecter->setObjectName("Connecter");
        Connecter->setGeometry(QRect(140, 30, 91, 21));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName("label_4");
        label_4->setGeometry(QRect(280, 450, 21, 17));
        tableView = new QTableView(centralwidget);
        tableView->setObjectName("tableView");
        tableView->setGeometry(QRect(20, 100, 721, 341));
        Effacer = new QPushButton(centralwidget);
        Effacer->setObjectName("Effacer");
        Effacer->setGeometry(QRect(510, 60, 99, 27));
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName("label_7");
        label_7->setGeometry(QRect(430, 450, 21, 17));
        OuvrirCAN = new QPushButton(centralwidget);
        OuvrirCAN->setObjectName("OuvrirCAN");
        OuvrirCAN->setGeometry(QRect(370, 30, 99, 27));
        FermerCAN = new QPushButton(centralwidget);
        FermerCAN->setObjectName("FermerCAN");
        FermerCAN->setGeometry(QRect(370, 60, 101, 23));
        label_9 = new QLabel(centralwidget);
        label_9->setObjectName("label_9");
        label_9->setGeometry(QRect(530, 450, 21, 17));
        DLC = new QSpinBox(centralwidget);
        DLC->setObjectName("DLC");
        DLC->setGeometry(QRect(150, 470, 41, 27));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(230, 450, 21, 17));
        label_10 = new QLabel(centralwidget);
        label_10->setObjectName("label_10");
        label_10->setGeometry(QRect(580, 450, 21, 17));
        Deconnecter = new QPushButton(centralwidget);
        Deconnecter->setObjectName("Deconnecter");
        Deconnecter->setGeometry(QRect(140, 60, 89, 25));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName("label_5");
        label_5->setGeometry(QRect(330, 450, 21, 17));
        PortCom = new QComboBox(centralwidget);
        PortCom->setObjectName("PortCom");
        PortCom->setGeometry(QRect(0, 30, 131, 27));
        D5 = new QLineEdit(centralwidget);
        D5->setObjectName("D5");
        D5->setGeometry(QRect(470, 470, 41, 27));
        Identifiant = new QLineEdit(centralwidget);
        Identifiant->setObjectName("Identifiant");
        Identifiant->setGeometry(QRect(20, 470, 113, 27));
        D6 = new QLineEdit(centralwidget);
        D6->setObjectName("D6");
        D6->setGeometry(QRect(520, 470, 41, 27));
        D0 = new QLineEdit(centralwidget);
        D0->setObjectName("D0");
        D0->setGeometry(QRect(220, 470, 41, 27));
        Extended = new QCheckBox(centralwidget);
        Extended->setObjectName("Extended");
        Extended->setGeometry(QRect(20, 500, 97, 22));
        MCP2515 = new QPushButton(centralwidget);
        MCP2515->setObjectName("MCP2515");
        MCP2515->setGeometry(QRect(690, 630, 99, 27));
        Envoie = new QPushButton(centralwidget);
        Envoie->setObjectName("Envoie");
        Envoie->setGeometry(QRect(650, 470, 99, 27));
        AcceptanceMask = new QPushButton(centralwidget);
        AcceptanceMask->setObjectName("AcceptanceMask");
        AcceptanceMask->setGeometry(QRect(10, 630, 141, 27));
        LineAcceptance = new QLineEdit(centralwidget);
        LineAcceptance->setObjectName("LineAcceptance");
        LineAcceptance->setGeometry(QRect(170, 630, 113, 27));
        Sauvegarder = new QPushButton(centralwidget);
        Sauvegarder->setObjectName("Sauvegarder");
        Sauvegarder->setGeometry(QRect(610, 60, 99, 27));
        D7 = new QLineEdit(centralwidget);
        D7->setObjectName("D7");
        D7->setGeometry(QRect(570, 470, 41, 27));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName("label_6");
        label_6->setGeometry(QRect(380, 450, 21, 17));
        Usbtin->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Usbtin);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 823, 23));
        Usbtin->setMenuBar(menubar);
        statusbar = new QStatusBar(Usbtin);
        statusbar->setObjectName("statusbar");
        Usbtin->setStatusBar(statusbar);

        retranslateUi(Usbtin);

        QMetaObject::connectSlotsByName(Usbtin);
    } // setupUi

    void retranslateUi(QMainWindow *Usbtin)
    {
        Usbtin->setWindowTitle(QCoreApplication::translate("Usbtin", "Usbtin", nullptr));
        AcceptanceFiltre->setText(QCoreApplication::translate("Usbtin", "SetAcceptanceFiltre", nullptr));
        label_2->setText(QCoreApplication::translate("Usbtin", "DLC", nullptr));
        label_8->setText(QCoreApplication::translate("Usbtin", "D5", nullptr));
        Rtr->setText(QCoreApplication::translate("Usbtin", "Rtr", nullptr));
        label->setText(QCoreApplication::translate("Usbtin", "Identifiant", nullptr));
        Connecter->setText(QCoreApplication::translate("Usbtin", "Connecter", nullptr));
        label_4->setText(QCoreApplication::translate("Usbtin", "D1", nullptr));
        Effacer->setText(QCoreApplication::translate("Usbtin", "Effacer", nullptr));
        label_7->setText(QCoreApplication::translate("Usbtin", "D4", nullptr));
        OuvrirCAN->setText(QCoreApplication::translate("Usbtin", "Ouvrir CAN", nullptr));
        FermerCAN->setText(QCoreApplication::translate("Usbtin", "Fermer CAN", nullptr));
        label_9->setText(QCoreApplication::translate("Usbtin", "D6", nullptr));
        label_3->setText(QCoreApplication::translate("Usbtin", "D0", nullptr));
        label_10->setText(QCoreApplication::translate("Usbtin", "D7", nullptr));
        Deconnecter->setText(QCoreApplication::translate("Usbtin", "Deconnecter", nullptr));
        label_5->setText(QCoreApplication::translate("Usbtin", "D2", nullptr));
        Extended->setText(QCoreApplication::translate("Usbtin", "Extended", nullptr));
        MCP2515->setText(QCoreApplication::translate("Usbtin", "RegMCP2515", nullptr));
        Envoie->setText(QCoreApplication::translate("Usbtin", "Envoie Data", nullptr));
        AcceptanceMask->setText(QCoreApplication::translate("Usbtin", "SetAcceptanceMask", nullptr));
        Sauvegarder->setText(QCoreApplication::translate("Usbtin", "Sauvegarder", nullptr));
        label_6->setText(QCoreApplication::translate("Usbtin", "D3", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Usbtin: public Ui_Usbtin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USBTIN_H
