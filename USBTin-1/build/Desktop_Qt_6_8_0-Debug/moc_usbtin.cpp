/****************************************************************************
** Meta object code from reading C++ file 'usbtin.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../usbtin.h"
#include <QtCore/qmetatype.h>

#include <QtCore/qtmochelpers.h>

#include <memory>


#include <QtCore/qxptype_traits.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'usbtin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSUsbtinENDCLASS_t {};
constexpr auto qt_meta_stringdata_CLASSUsbtinENDCLASS = QtMocHelpers::stringData(
    "Usbtin",
    "onRecu",
    "",
    "slotOpenCAN",
    "slotCloseCAN",
    "slotVitesseCAN",
    "onConfirm",
    "slotLireMCP2515",
    "reg",
    "on_Connecter_clicked",
    "on_Deconnecter_clicked",
    "on_Vitesse_activated",
    "index",
    "on_Sauvegarder_clicked",
    "on_Effacer_clicked",
    "on_AcceptanceMask_clicked",
    "on_AcceptanceFiltre_clicked",
    "on_MCP2515_clicked",
    "on_OuvrirCAN_clicked",
    "on_FermerCAN_clicked",
    "on_Envoie_clicked",
    "on_EnvoieData_clicked",
    "on_Extended_stateChanged",
    "arg1",
    "on_Rtr_stateChanged"
);
#else  // !QT_MOC_HAS_STRINGDATA
#error "qtmochelpers.h not found or too old."
#endif // !QT_MOC_HAS_STRINGDATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSUsbtinENDCLASS[] = {

 // content:
      12,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  134,    2, 0x08,    1 /* Private */,
       3,    0,  135,    2, 0x08,    2 /* Private */,
       4,    0,  136,    2, 0x08,    3 /* Private */,
       5,    0,  137,    2, 0x08,    4 /* Private */,
       6,    0,  138,    2, 0x08,    5 /* Private */,
       7,    1,  139,    2, 0x08,    6 /* Private */,
       9,    0,  142,    2, 0x08,    8 /* Private */,
      10,    0,  143,    2, 0x08,    9 /* Private */,
      11,    1,  144,    2, 0x08,   10 /* Private */,
      13,    0,  147,    2, 0x08,   12 /* Private */,
      14,    0,  148,    2, 0x08,   13 /* Private */,
      15,    0,  149,    2, 0x08,   14 /* Private */,
      16,    0,  150,    2, 0x08,   15 /* Private */,
      17,    0,  151,    2, 0x08,   16 /* Private */,
      18,    0,  152,    2, 0x08,   17 /* Private */,
      19,    0,  153,    2, 0x08,   18 /* Private */,
      20,    0,  154,    2, 0x08,   19 /* Private */,
      21,    0,  155,    2, 0x08,   20 /* Private */,
      22,    1,  156,    2, 0x08,   21 /* Private */,
      24,    1,  159,    2, 0x08,   23 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UChar,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   23,
    QMetaType::Void, QMetaType::Int,   23,

       0        // eod
};

Q_CONSTINIT const QMetaObject Usbtin::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_CLASSUsbtinENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSUsbtinENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSUsbtinENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<Usbtin, std::true_type>,
        // method 'onRecu'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'slotOpenCAN'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'slotCloseCAN'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'slotVitesseCAN'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'onConfirm'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'slotLireMCP2515'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<unsigned char, std::false_type>,
        // method 'on_Connecter_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Deconnecter_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Vitesse_activated'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<int, std::false_type>,
        // method 'on_Sauvegarder_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Effacer_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_AcceptanceMask_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_AcceptanceFiltre_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_MCP2515_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_OuvrirCAN_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_FermerCAN_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Envoie_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_EnvoieData_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Extended_stateChanged'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<int, std::false_type>,
        // method 'on_Rtr_stateChanged'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<int, std::false_type>
    >,
    nullptr
} };

void Usbtin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Usbtin *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->onRecu(); break;
        case 1: _t->slotOpenCAN(); break;
        case 2: _t->slotCloseCAN(); break;
        case 3: _t->slotVitesseCAN(); break;
        case 4: _t->onConfirm(); break;
        case 5: _t->slotLireMCP2515((*reinterpret_cast< std::add_pointer_t<uchar>>(_a[1]))); break;
        case 6: _t->on_Connecter_clicked(); break;
        case 7: _t->on_Deconnecter_clicked(); break;
        case 8: _t->on_Vitesse_activated((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 9: _t->on_Sauvegarder_clicked(); break;
        case 10: _t->on_Effacer_clicked(); break;
        case 11: _t->on_AcceptanceMask_clicked(); break;
        case 12: _t->on_AcceptanceFiltre_clicked(); break;
        case 13: _t->on_MCP2515_clicked(); break;
        case 14: _t->on_OuvrirCAN_clicked(); break;
        case 15: _t->on_FermerCAN_clicked(); break;
        case 16: _t->on_Envoie_clicked(); break;
        case 17: _t->on_EnvoieData_clicked(); break;
        case 18: _t->on_Extended_stateChanged((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 19: _t->on_Rtr_stateChanged((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject *Usbtin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Usbtin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSUsbtinENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Usbtin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
