/*
 * device.cpp
 *
 *  Created on: 2 avr. 2016
 *      Author: michel arnaud  (michel.arnaud1@orange.fr)
 *
 *  Copyright (C) 2016  michel arnaud  (michel.arnaud1@orange.fr)
 *  @file         BusCAN.cpp
 *  @brief
 *  @version      0.1
 *  @date         06 septembre 2019 22:40:52
 *
 *  Description detaillee du fichier BusCAN.cpp
 *  Fabrication   gcc (Ubuntu 5.9.5-2ubuntu1~18.04.2) QT5.9.5
 *  @todo         Liste des choses restant a faire.
 *  @bug          30 mars 2016 09:40:52 - Aucun pour l'instant
 */
#include "device.h"
#include <QProcess>
#include <QDebug>

Device::Device() {
	// TODO Auto-generated constructor stub

}

Device::~Device() {
	// TODO Auto-generated destructor stub
}

 bool Device::getDevices(QVector<QString> * dev){
	int i,j;
	bool trouvedevice=false;
		char val;
		QByteArray devUSB,devACM;
		QString devusb;
		//QVector<QString> dev;
	    QProcess* process = new QProcess();
	    QStringList sArgs;
        process->startCommand("sh -c \"ls /dev/ttyU*\"");
	    process->waitForFinished(-1);
	   // qDebug() << "hello;" ;
	 devUSB= process->readAllStandardOutput();
	 i=0;
	 while (i<devUSB.length()){
		 j=0;
		while(devUSB[i]!=0xa){
			val=devUSB[i];
			devusb+=val;
			i++;j++;
		}
		i++;trouvedevice=true;
		dev->push_back(devusb);
	 	devusb.clear();
	}
     process->startCommand("sh -c \"ls /dev/ttyA*\"");
	 process->waitForFinished(-1);
	// qDebug() << "hello;" ;
	devUSB= process->readAllStandardOutput();
	i=0;
	while (i<devUSB.length()){
		 j=0;
		while(devUSB[i]!=0xa){
			val=devUSB[i];
			devusb+=val;
			i++;j++;
		}
		i++;trouvedevice=true;
		dev->push_back(devusb);
		devusb.clear();
	}
	return trouvedevice;
}
