/*
 * BusCAN.cpp
 *
 *  Created on: 20 déc. 2015
 *      Author: michel
 */

#include "BusCAN.h"
#include <stdio.h>
//SerialPort::SerialPort(QObject *parent) :
//QSerialPort(parent) {
#define IDLENGTHSTD 3
#define IDLENGTHEXT 8

BusCAN::BusCAN(QObject *parent): QObject(parent)
//BusCAN::BusCAN()
{
	// TODO Auto-generated constructor stub
	mport=new SerialPort(this);
 	connect(mport,SIGNAL(readyRead()),this,SLOT(RecevoirMessage()));
	typecan=STANDART;

	open=false;
}


BusCAN::~BusCAN() {
	// TODO Auto-generated destructor stub
     if (mport->isOpen()){
    	 mport->write("C\015");
    	delete mport;
     }


}

void BusCAN::Fermer(){
    if (mport->isOpen()){
   	 mport->write("C\015");
    }

}

bool BusCAN::openCAN( QString *dev){

	mdev=dev;

	  open=mport->setOpenPort(mdev);


	  return open;
}

void BusCAN::ouvrirCAN(int vitesse){
	char	Message[30];
	switch (vitesse){
		case 10:	sprintf(Message,"S0\015");	break;
		case 20:	sprintf(Message,"S1\015");	break;
		case 50:	sprintf(Message,"S2\015");	break;
		case 100:	sprintf(Message,"S3\015");	break;
		case 125:	sprintf(Message,"S4\015");	break;
		case 250:	sprintf(Message,"S5\015");	break;
		case 500:	sprintf(Message,"S6\015");	break;
		case 800:	sprintf(Message,"S7\015");	break;
		case 1000:	sprintf(Message,"S8\015");	break;

	}
	  if (mport->isOpen()){
		  mport->write(Message);

		  /*  do{
		    	mport->getNextByte();
		    	n=mport->bytesAvailable();
		    }while(n>0);*/
		    mport->write("O\015");
			    open=true;
	  }
}

char inttoASCII(int a, int n){
	char retour;
	int masq=0xF<<(n*4);
	int val=(a&masq)>>(n*4);
	if (val<10)
		retour=(char)(val+'0');
		else
			retour=(char)((val-10)+'A');
	return retour;
}

/*
 * Envoie d'un message can
 */
bool BusCAN::EnvoyerMessage(TMessage *mess){
	char message[50]={'t'};
	int id=mess->id;
	int i,indice,idlength;;
	QByteArray bytearid;
	bytearid.setNum(id,16);
	if (mess->typem==STANDART){
		idlength=IDLENGTHSTD;
	}
	else {
		idlength=IDLENGTHEXT;
		message[0]='T';
	}
		while (bytearid.length()<idlength)
			bytearid.insert(0,'0');
	for(indice=0;indice<idlength;indice++){
			message[indice+1]=(char)(bytearid[indice]);
	}
	indice++;
	message[indice++]=mess->length+0x30;
	for(i=0;i<mess->length;i++){
		if (mess->donnee[i].length()>1){
			message[indice++]=mess->donnee[i].at(0);
			message[indice++]=mess->donnee[i].at(1);
		}
		else {
			message[indice++]=0x30;
			message[indice++]=mess->donnee[i].at(0);
		}
	}
	message[indice++]='\015';
	message[indice]='\0';
	mport->write(message);
}

void BusCAN::RecevoirMessage(){
	char messageretour[50];
	bool messr=true;
	int idlength;
	int i=0;
	int id=0;
	int l=0;
	int j=0;
	int a=mport->read(messageretour,50);
	if (a>0){
		switch (messageretour[0]){
		case 't':	messagelu.typem=STANDART;break;
		case 'T':	messagelu.typem=EXTENDED;break;
		default :messr=false;
		}
   if (messr){
	   if (messagelu.typem==STANDART){
			idlength=IDLENGTHSTD;
		}
		else  idlength=IDLENGTHEXT;

		   while ((messageretour[i]!='\015')&&(i<=idlength)){
			   if ((messageretour[i]>=0x30)&&(messageretour[i]<='9')){
				   id = id*16 + messageretour[i]-0x30;
			   }
			   if ((messageretour[i]>='A')&&(messageretour[i]<='F')){
					id = id*16 + messageretour[i]-0x37;
				}
			   i++;
		   }

	messagelu.id=id;
	messagelu.length=messageretour[i++]-0x30;
	while ((messageretour[i]!='\015')&&(l<=8)){
        messagelu.donnee[l].push_back(messageretour[i]);
		i++;j++;
		if (j>=2){
			j=0;l++;
		}
	}
	mport->flush();
	emit (pourLire());
	}
	}
	else {
		mport->flush();
		emit confirm();
	}
}

TMessage BusCAN::getMessage(){
	return messagelu;
}

char BusCAN::getEtat(){
	return 15;
}
