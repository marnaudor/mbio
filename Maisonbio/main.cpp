#include "maisonbio.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MaisonBio w;
    w.show();
    return a.exec();
}
