#ifndef MAISONBIO_H
#define MAISONBIO_H

#include <QMainWindow>
#include "BusCAN.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MaisonBio;
}
QT_END_NAMESPACE

class MaisonBio : public QMainWindow
{
    Q_OBJECT
    bool opencan,openport;
    BusCAN *mcan;

public:
    MaisonBio(QWidget *parent = nullptr);
    ~MaisonBio();
public slots:
    void EcrireIHM();
private:
    Ui::MaisonBio *ui;
};
#endif // MAISONBIO_H
