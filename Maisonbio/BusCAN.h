/*
 * BusCAN.h
 *
 *  Created on: 20 déc. 2015
 *      Author: michel
 */

#ifndef BUSCAN_H_
#define BUSCAN_H_
#include "SerialPort.h"
#include <QObject>
#include <QtCore>

enum type {STANDART,EXTENDED};

typedef struct TMessage{
	enum type typem;
	int id;
	int length;
    QByteArray donnee[8];
}TM;


class BusCAN: public QObject
{
	 Q_OBJECT

	SerialPort *mport;
	QString *mdev;
	int mavitesse;
	bool open;
	TMessage messagelu;
	enum type typecan;
public:
	BusCAN(QObject *parent = 0);
	//BusCAN();
	virtual ~BusCAN();
	bool openCAN( QString *dev);
	bool EnvoyerMessage(TMessage *mess);
	TMessage getMessage();
	void Fermer();
	void ouvrirCAN(int vitesse);
	char getEtat();
public slots:
	void RecevoirMessage();
signals:
	void pourLire();
	void confirm();
};

#endif /* BUSCAN_H_ */
