/*
 * SerialPort.h
 *
 *  Created on: 9 févr. 2015
 *      Author: michel
 */

#ifndef SERIALPORT1_H_
#define SERIALPORT1_H_
#include <QSerialPort>

class SerialPort:public QSerialPort {
	 Q_OBJECT
	 bool openport;
public:
	SerialPort(QObject *parent = 0);
	virtual ~SerialPort();
    unsigned char getNextByte();
    bool setOpenPort(QString *port);
    // return number of bytes in receive buffer
  //  unsigned int bytesAvailable();
    bool getOpen();
    // write buffer
//    int writeBuf(QByteArray *buffer);
    int writeBuf( char *buf,int l   );

 //   int OuvrirPort();

    QByteArray *dataBuffer;

};

#endif /* SERIALPORT1_H_ */
